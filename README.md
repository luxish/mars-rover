# Mars Rover

Mars Rover is a project that handles a mission scenario that depoys a number of mechanical robots (rovers) in order to explore the Mars surface in detail.

- **Mars Surface** is a 2D representation of the Mars surface. It does not have any obstacles (other than the actual rovers).

- **Rover** is a mechanical robot that can be maneuvered from a mission control point. The rover can turn right or left and move forward.

## Assumptions

Collisions can occur when the rovers are deployed or moving. In case the deployment collides with another one, just the first rover will be operational when the scenario runs (we can assume that NASA gets the rover off the map to avoid the destruction of both robots).

When rovers avoid colliding with each other they just skip the `move` command. The chain of commands continues.

Rovers can not go off the map. If a rover tries to leave the map, the command will be handled the same as a collision.

## Build it

The project can be build using Gradle. This tool was chosen because you can write plugins faster and the build file has a clean look.  

```bash
gradlew clean build
```

The built jars can be found at `mars-rover/build/libs` and it consists of two jars `mars-rover.jar` and `remote-client.jar`

## Run it
The input argumests rules:

- First 2 arguments should be the surface initialization parameters
- The next arguments should be pairs of 4
- the pairs of 4 should contain `( position_x_int , position_y_int, direction_string, commands_string )`
- The direction string should be one of those: `N E S W`
- The commands string should contain any sequence of these specific commands: `L` - turn left  `R` - turn right `M` - move forward. 

### Local
The built `mars-rover.jar` jar can be executed in terminal like this:

```bash
java -jar <path-to>/mars-rover.jar <input args>
```
A more specific example:
```bash
java -jar mars-rover.jar 5 5 1 1 N MMR 2 2 M
```

### Remote

To run the project in a remote way you need to run the `mars-rover.jar` with the `remote` property set on true to become the host. The port can be also set by setting the `port` property (the default port is `8888`).:

```
java -Dremote=true -jar <path-to>/mars-rover.jar
```

One of the built jars is `remote-client.jar` and this can be used to connect the host that has the mission control. The target host can be specified using the `target` property. If the property is not set, the client will try to connect to the default taget `localhost:8888` :
```
java -jar remote-client.jar 5 5 1 1 N MMR 2 2 M
```

```
java -Dtarget=localhost:8777 -jar remote-client.jar 5 5 1 1 N MMR 2 2 M
```
