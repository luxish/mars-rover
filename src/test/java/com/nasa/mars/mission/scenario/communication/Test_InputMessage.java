package com.nasa.mars.mission.scenario.communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.nasa.mars.mission.scenario.communication.InputMessage;
import com.nasa.mars.mission.scenario.communication.InputMessageDecoder;
import com.nasa.mars.mission.scenario.communication.InputMessage.InputMessageChunk;

public class Test_InputMessage {

	@Test
	public void test_parseMessage_validArgs() throws Exception {
		String[] input =  "5 6 1 2 E ASDFASD 2 3 N LLLRRRMMMM".trim().split(" ");
		InputMessage message = InputMessageDecoder.parseMessage(input);
		
		assertNotNull(message);
		
		assertNotNull(message.getInitMapParams());
		assertEquals(2, message.getInitMapParams().length);
		assertEquals(5, message.getInitMapParams()[0]);
		assertEquals(6, message.getInitMapParams()[1]);
		
		assertNotNull(message.getChunks());
		assertEquals(2, message.getChunks().size());
		
		InputMessageChunk chunk = null;
		chunk = message.getChunks().get(0);
		assertEquals(1, chunk.posX);
		assertEquals(2, chunk.posY);
		assertEquals("E", chunk.direction);
		assertEquals("ASDFASD", chunk.commands);
		
		chunk = message.getChunks().get(1);
		assertEquals(2, chunk.posX);
		assertEquals(3, chunk.posY);
		assertEquals("N", chunk.direction);
		assertEquals("LLLRRRMMMM", chunk.commands);
	}
	
	@Test
	public void test_parseMessage_invalidArgsNoMapParameters() throws Exception {
		String[] input =  "1 2 E ASDFASD 2 3 N LLLRRRMMMM".trim().split(" ");
		Exception exception = null;
		try {
			InputMessageDecoder.parseMessage(input);
		} catch (Exception ex) {
			exception = ex;
		}
		assertNotNull("Expected to fail with an exception.", exception);
		assertTrue(exception instanceof IllegalArgumentException);
	}
	
	@Test
	public void test_parseMessage_invalidArgsNoRoversParameters() throws Exception {
		String[] input =  "1 2".trim().split(" ");
		Exception exception = null;
		try {
			InputMessageDecoder.parseMessage(input);
		} catch (Exception ex) {
			exception = ex;
		}
		assertNotNull("Expected to fail with an exception.", exception);
		assertTrue(exception instanceof IllegalArgumentException);
	}
	
	@Test
	public void test_parseMessage_invalidArgsIncompleteChunk() throws Exception {
		String[] input =  "5 6 1 2 E ASDFASD 2 3 ".trim().split(" ");
		Exception exception = null;
		try {
			InputMessageDecoder.parseMessage(input);
		} catch (Exception ex) {
			exception = ex;
		}
		assertNotNull("Expected to fail with an exception.", exception);
		assertTrue(exception instanceof IllegalArgumentException);
	}
	
	@Test
	public void test_parseMessage_invalidArgsInvalidInts() throws Exception {
		String[] input =  "5 6 A B E ASDFASD".trim().split(" ");
		Exception exception = null;
		try {
			InputMessageDecoder.parseMessage(input);
		} catch (Exception ex) {
			exception = ex;
		}
		assertNotNull("Expected to fail with an exception.", exception);
		assertTrue(exception instanceof IllegalArgumentException);
	}
}
