package com.nasa.mars.mission.scenario.discovery;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.nasa.mars.mission.location.Direction;
import com.nasa.mars.mission.location.Position;
import com.nasa.mars.mission.location.SurfaceMap;
import com.nasa.mars.mission.scenario.DeploymentScenario;
import com.nasa.mars.mission.scenario.communication.OutputMessage;
import com.nasa.mars.mission.scenario.discovery.robot.MRover;

/**
 * Tests for {@link DiscoveryDeploymentScenario}. 
 * 
 * @author Lucian M
 *
 */
public class Test_DiscoveryDeploymentScenario {
	
	/**
	 * Test that would successfully return the new positions after the commands were processed.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deploymentScenario_successfull() throws Exception {
		
		DeploymentScenario scenario = new  DiscoveryDeploymentScenario(new SurfaceMap(5,  5));
		scenario.addRover(new MRover(new Position(1, 2), Direction.N), "LMLMLMLMM");
		scenario.addRover(new MRover(new Position(3, 3), Direction.E), "MMRMMRMRRM");
		
		OutputMessage result = scenario.runExpeditions();
		assertNotNull(result);
		assertEquals("1 3 N 5 1 E", result.toString());
	}
	
	/**
	 * Test that would encounter a collision. The collision is printed on the console and the specific
	 * move is not performed. The second rover should have the same position as the init position. 
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deploymentScenario_collision() throws Exception {
		DeploymentScenario scenario = new  DiscoveryDeploymentScenario(new SurfaceMap(5,  5));
		scenario.addRover(new MRover(new Position(1, 2), Direction.N), "");
		scenario.addRover(new MRover(new Position(2, 2), Direction.W), "M");
		
		OutputMessage result = scenario.runExpeditions();
		assertNotNull(result);
		assertEquals("1 2 N 2 2 W", result.toString());
	}
	
	/**
	 * Test that would encounter a collision. The collision is printed on the console and the specific
	 * move is not performed. The rover should have the same position as the init position since it wants
	 * to move off the map.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deploymentScenario_offTheMap() throws Exception {
		DeploymentScenario scenario = new  DiscoveryDeploymentScenario(new SurfaceMap(5,  5));
		scenario.addRover(new MRover(new Position(0, 0), Direction.N), "LM");
		
		OutputMessage result = scenario.runExpeditions();
		assertNotNull(result);
		assertEquals("0 0 W", result.toString());
	}

}
