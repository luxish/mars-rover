package com.nasa.mars.mission.scenario.discovery;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.nasa.mars.mission.scenario.DeploymentScenario;
import com.nasa.mars.mission.scenario.communication.InputMessage;
import com.nasa.mars.mission.scenario.communication.InputMessageDecoder;
import com.nasa.mars.mission.scenario.communication.OutputMessage;
import com.nasa.mars.mission.scenario.discovery.MissionScenarioRunner;

/**
 * High level tests for the mission control.
 * 
 * @author Lucian M
 *
 */
public class Test_MissionControlDispatcher {

	/**
	 * Test with two rovers that would complete without collisions.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_missionControl_success() throws Exception {
		
		String inputString = "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM";
		InputMessage inputMessage = InputMessageDecoder.parseMessage(inputString);

		MissionScenarioRunner dispatcher = new MissionScenarioRunner();
		DeploymentScenario scenario = dispatcher.setupDeploymentScenario(inputMessage);
		
		OutputMessage result = scenario.runExpeditions();
		
		assertNotNull(result);
		assertEquals("1 3 N 5 1 E", result.toString());
	}
	
	/**
	 * Test with two rovers that would be initialized on the same position. Only the first rover 
	 * will be maneuvered. Initializing on the same spot on the map should not considered intended.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_missionControl_initCollision() throws Exception {
		
		String inputString = "5 5 1 2 N LMLMLMLMM 1 2 E MMRMMRMRRM";
		InputMessage inputMessage = InputMessageDecoder.parseMessage(inputString);

		MissionScenarioRunner dispatcher = new MissionScenarioRunner();
		DeploymentScenario scenario = dispatcher.setupDeploymentScenario(inputMessage);
		
		OutputMessage result = scenario.runExpeditions();
		
		assertNotNull(result);
		assertEquals("1 3 N", result.toString());
	}
	
	/**
	 * Test with two rovers that would be initialized off the map. The rovers will not reach
	 * the scenario (as actors) because the scenario is map based. 
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_missionControl_initOffTheMap() throws Exception {
		
		String inputString = "5 5 -1 -1 N LMLMLMLMM -1 -2 E MMRMMRMRRM";
		InputMessage inputMessage = InputMessageDecoder.parseMessage(inputString);

		MissionScenarioRunner dispatcher = new MissionScenarioRunner();
		DeploymentScenario scenario = dispatcher.setupDeploymentScenario(inputMessage);
		
		OutputMessage result = scenario.runExpeditions();
		
		assertNotNull(result);
		assertEquals("", result.toString());
	}
	
	/**
	 * The first rover has some commands attached however those are ignored.  The second rover has known and 
	 * unknown commands and only the known ones are taken into consideration. 
	 * The only commands known are L (turn left) R (turn right)  M (move).
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_missionControl_commandsUnknown() throws Exception {
		
		String inputString = "5 5 1 2 N IDK 3 3 E MMRMMRMRRMI";
		InputMessage inputMessage = InputMessageDecoder.parseMessage(inputString);

		MissionScenarioRunner dispatcher = new MissionScenarioRunner();
		DeploymentScenario scenario = dispatcher.setupDeploymentScenario(inputMessage);
		
		OutputMessage result = scenario.runExpeditions();
		
		assertNotNull(result);
		assertEquals("1 2 N 5 1 E", result.toString());
	}
	
	/**
	 * Test with a rover set with an unknown direction. The commands depend on the position 
	 * of the rover so at this point the rover can not be operational.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_missionControl_directionUnknown() throws Exception {
		
		String inputString = "5 5 1 2 Z LMLMLMLMM 3 3 E MMRMMRMRRM";
		InputMessage inputMessage = InputMessageDecoder.parseMessage(inputString);

		MissionScenarioRunner dispatcher = new MissionScenarioRunner();
		DeploymentScenario scenario = dispatcher.setupDeploymentScenario(inputMessage);
		
		OutputMessage result = scenario.runExpeditions();
		
		assertNotNull(result);
		assertEquals("5 1 E", result.toString());
	}
	
	
}
