package com.nasa.mars.mission.control;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Remote mission control initializer. This way the scenario can be remotely run
 * using socket connections. This implementation of {@link MissionControl} creates
 * a socket server and listens to incoming connections.  
 * 
 * @author Lucian M
 *
 */
public class RemoteMissionControl implements MissionControl {
	
	private int port;
	
	ServerSocket socketServer;

	/**
	 * Creates an instance of {@link RemoteMissionControl} with a predefined port.
	 * 
	 * @param port
	 */
	public RemoteMissionControl(int port) {
		this.port = port;
	}

	/**
	 * {@inheritDoc}
	 */
	public void start() {
		if(socketServer != null) {
			System.out.println("Socket server already started.");
			return ;
		}
		try {
			socketServer = new ServerSocket(port);
			System.out.println("Listening on port " + port);
			while (!socketServer.isClosed()) {
				new RemoteConnectionHandler(socketServer.accept()).handle();
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void stop() {
		try {
			if (socketServer != null) {
				socketServer.close();
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
