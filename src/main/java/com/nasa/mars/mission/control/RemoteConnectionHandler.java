package com.nasa.mars.mission.control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.Charset;

import com.nasa.mars.mission.scenario.DeploymentScenario;
import com.nasa.mars.mission.scenario.communication.InputMessage;
import com.nasa.mars.mission.scenario.communication.InputMessageDecoder;
import com.nasa.mars.mission.scenario.communication.OutputMessage;
import com.nasa.mars.mission.scenario.discovery.MissionScenarioRunner;

/**
 * Class used to handle the remote communication and run the mission scenario. 
 * 
 * @author Lucian M
 *
 */
public class RemoteConnectionHandler {

	private static final Charset UTF_8 = Charset.forName("UTF8");
	private final Socket connection;

	public RemoteConnectionHandler(Socket connection) {
		this.connection = connection;
	}
	
	/**
	 * Main handler method.
	 */
	public void handle() {
		try {
			System.out.println("Connection established");
			InputMessage inputMessage = getInputMessage();
			if(inputMessage == null) {
				return;
			}
			MissionScenarioRunner dispatcher = new MissionScenarioRunner();
			DeploymentScenario deployment = dispatcher.setupDeploymentScenario(inputMessage);
			
			// Run the expeditions using the deployed rovers.
			OutputMessage result = deployment.runExpeditions();
			
			writeOutputToConnection(result);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				System.out.println("Connection closed");
				connection.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private InputMessage getInputMessage() throws IOException {
		String inputString = readInputFromConnection();
		try {
			return InputMessageDecoder.parseMessage(inputString.split(" "));
		} catch (IllegalArgumentException ex) {
			// Ignore the exception. Do not stop the process.
		}
		return null;
	}

	/**
	 * Method used to write the result to the client.
	 * 
	 * @param result {@link OutputMessage}
	 * @throws IOException
	 */
	private void writeOutputToConnection(OutputMessage result) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), UTF_8));
		writer.write(result.toString());
		writer.flush();
	}

	/**
	 * Method used to retrieve the input from the client.
	 * 
	 * @return {@link String} input
	 * @throws IOException
	 */
	private String readInputFromConnection() throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), UTF_8));
		return reader.readLine();
	}
	
}
