package com.nasa.mars.mission.control;

/**
 * Mission control abstract behavior. The mission control can take many forms as
 * it can handle different types of communication or custom behavior based on 
 * other dependencies.
 * 
 * @author Lucian M
 *
 */
public interface MissionControl {

	/**
	 * Method used to start the mission.
	 */
	void start();

}
