package com.nasa.mars.mission.control;

import com.nasa.mars.mission.scenario.DeploymentScenario;
import com.nasa.mars.mission.scenario.communication.InputMessage;
import com.nasa.mars.mission.scenario.communication.InputMessageDecoder;
import com.nasa.mars.mission.scenario.communication.OutputMessage;
import com.nasa.mars.mission.scenario.discovery.MissionScenarioRunner;

/**
 * Mission control implementation that takes the input from string arguments and 
 * prints the result.
 * 
 * @author Lucian M
 *
 */
public class LocalMissionControl implements MissionControl {
	
	private String[] inputArray;
	
	public LocalMissionControl(String... args) {
		inputArray = args;
	}

	@Override
	public void start() {
		InputMessage inputMessage = InputMessageDecoder.parseMessage(inputArray);
		
		// Setup the scenario for the provided input.
		MissionScenarioRunner dispatcher = new MissionScenarioRunner();
		DeploymentScenario deployment = dispatcher.setupDeploymentScenario(inputMessage);
		
		// Run the expeditions using the deployed rovers.
		OutputMessage result = deployment.runExpeditions();
		
		System.out.println(result);
	}

}
