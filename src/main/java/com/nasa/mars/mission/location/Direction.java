package com.nasa.mars.mission.location;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enumeration of the possible directions. In this case those coincide with the
 * cardinal locations. 
 * 
 * @author Lucian M
 */
public enum Direction {
	N(0), 
	E(1), 
	S(2), 
	W(3);
	
	/**
	 * Method used to retrieve the {@link Direction} enumeration value based on the code.
	 * 
	 * @param code int direction code.
	 * @return {@link Direction}
	 */
	public static Direction getDirectionByCode(int code) {
		int normalizedCode = normalizeDirectionCode(code);
		Optional<Direction> direction = 
				Arrays.stream(Direction.values()).filter(dir ->  dir.getCode() == normalizedCode).findFirst(); 
		return direction.orElse(null);
	}
	
	private static int normalizeDirectionCode(int code) {
		return code < 0 ? 4 + code : code % 4;
	}
	
	int code;
	
	private Direction(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
