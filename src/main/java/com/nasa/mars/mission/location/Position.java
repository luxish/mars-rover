package com.nasa.mars.mission.location;

/**
 * Class used to set position related data in a 2D environment. Position objects are immutable.
 * 
 * @author Lucian M
 */
public class Position {
	
	private final int x;
	
	private final int y;
	
	/**
	 * Constructs a position object.
	 * 
	 * @param x the horizontal position in a map. 
	 * @param y the vertical position in a map.
	 * @param direction {@link Direction} the direction that the object is facing.
	 */
	public Position(int x, int y) {
		this.x = x;
		this.y= y;
	}
	
	/**
	 * Gets the horizontal position.
	 * 
	 * @return integer value
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Gets the vertical position.
	 * @return integer value
	 */
	public int getY() {
		return y;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;			
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;			
		}
		Position other = (Position) obj;
		if (x != other.x || y != other.y) {
			return false;			
		}
		return true;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
}
