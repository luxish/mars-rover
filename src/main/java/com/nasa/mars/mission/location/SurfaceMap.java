package com.nasa.mars.mission.location;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to store details about the 2D surface map. 
 * 
 * @author Lucian M
 */
public class SurfaceMap {
	
	private final int maxY;
	
	private final int maxX;
	
	private final List<Position> obstaclesList;

	public SurfaceMap(int width, int height) {
		this.maxX = width;
		this.maxY = height;
		this.obstaclesList = new ArrayList<>();
	}

	/**
	 * Method used to mark the position of obstacles.
	 * 
	 * @param position {@link Position}
	 */
	public void markPositionTaken(Position position) {
		obstaclesList.add(position);
	}

	/**
	 * Method used to check if a position is in a collision or off the map scenario.
	 * 
	 * @param position {@link Position}
	 * @return
	 */
	public Boolean hasCollision(Position position) {
		return isPositionOutOfBounds(position) || isPositionTaken(position);
	}

	/**
	 * Checks if the provided position is out of bounds.
	 * 
	 * @param position {@link Position}
	 * @return boolean
	 */
	private boolean isPositionOutOfBounds(Position position) {
		return position.getX() < 0 || position.getX() > maxX  || 
				position.getY() < 0 || position.getY() > maxY;
	}
	
	/**
	 * Checks if the position is taken.
	 * 
	 * @param position {@link Position}
	 * @return boolean
	 */
	private boolean isPositionTaken(Position position) {
		return obstaclesList.contains(position);
	}
}
