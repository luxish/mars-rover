package com.nasa.mars.mission.location;

/**
 * Position utility class used for {@link Position} operations. 
 * 
 * @author Lucian M
 *
 */
public class PositionUtil {

	private PositionUtil() {}
	
	/**
	 * Offset position for the north direction.
	 */
	public static final Position NORTH_OFFSET = new Position(0,  1);
	
	/**
	 * Offset position for the south direction.
	 */
	public static final Position SOUTH_OFFSET = new Position(0,  -1);
	
	/**
	 * Offset position for the east direction.
	 */
	public static final Position EAST_OFFSET = new Position(1,  0);
	
	/**
	 * Offset position for the west direction.
	 */
	public static final Position WEST_OFFSET = new Position(-1,  0);
	
	/**
	 * Offset position for the current direction. This can be used as a void position object.
	 */
	public static final Position ZERO_OFFSET = new Position(0, 0);
	
	
	/**
	 *  Utility method used to calculate the position with a certain difference.
	 * 
	 * @param initialPosition {@link Position}
	 * @param diff {@link Position}
	 * @return {@link Position} the calculated position or the ZERO_OFFSET offset if none was found. 
	 */
	public static final Position createPositionWithDifference(Position initialPosition, Position diff) {
		if(initialPosition == null || diff == null) {
			return null;
		}
		return new Position(initialPosition.getX() + diff.getX(), initialPosition.getY() + diff.getY());
	}
	
	/**
	 * Method used to return the position offset based on a direction.
	 * 
	 * @param direction {@link Direction}
	 * @return {@link Position} the offset position, 
	 */
	public static final Position getDirectionOffset(Direction direction) {
		if (direction == Direction.E) {
			return EAST_OFFSET;
		} else if (direction == Direction.W) {
			return WEST_OFFSET;
		} else if (direction == Direction.N) {
			return NORTH_OFFSET;
		} else if (direction == Direction.S) {
			return SOUTH_OFFSET;
		}
		return ZERO_OFFSET;
	}
}
