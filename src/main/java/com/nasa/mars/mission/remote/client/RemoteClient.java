package com.nasa.mars.mission.remote.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Minimal remote client used to communicate with the mars-rover mission.
 * The jar can be run with the input arguments described in the specification.
 * 
 * Usage example: 
 * 		java -jar remote-client 7 7 3 3 N MLLMMR 
 *  	
 * @author Lucian M
 *
 */
public class RemoteClient {
	
	private static final String END_LINE = "\n";

	public static void main(String[] args) throws UnknownHostException, IOException {
		String[] input = extractConnectionData();
		String host = input[0];
		String port = input[1];
		Socket connection = new Socket(host, Integer.parseInt(port));
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
		writer.write(String.join(" ", args) + END_LINE);
		writer.flush();
		
		String response = reader.readLine() ;
		System.out.println(response != null ? response.toString() : "No response");
		
		connection.close();
	}

	private static String[] extractConnectionData() {
		String target = System.getProperty("target", "");
		String[] tokens = target.split(":");
		if(tokens.length != 2) {
			System.out.println("Connection setting are not valid or existent. The defaults are used: localhost:8888");
			return new String[] { "localhost", "8888" };
		}
		return tokens;
	}

}
