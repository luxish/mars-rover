package com.nasa.mars.mission;

import com.nasa.mars.mission.control.LocalMissionControl;
import com.nasa.mars.mission.control.MissionControl;
import com.nasa.mars.mission.control.RemoteMissionControl;

/**
 * Main entry point in the mission scenario mechanism.
 * 
 * @author Lucian M
 *
 */
public class Main {
	

	/**
	 * Main method of the mission control.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		boolean isRemote = getRemotePropertyFlag();
		MissionControl mc = isRemote ? getRemoteMissionControl() : getLocalMissionControl(args);
		mc.start();
		
	}

	/**
	 * Gets the "remote" defined property.
	 * 
	 * @return true if the mission can should take remote input.
	 */
	private static boolean getRemotePropertyFlag() {
		return Boolean.parseBoolean(System.getProperty("remote", "false"));
	}

	/**
	 * Creates a local mission control object based on the array of string arguments.
	 * 
	 * @param args String[]
	 * @return {@link LocalMissionControl}
	 */
	private static LocalMissionControl getLocalMissionControl(String[] args) {
		return new LocalMissionControl(args);
	}

	/**
	 * Creates a remote mission control object based on the provided properties.
	 * 
	 * @return {@link MissionControl}
	 */
	private static MissionControl getRemoteMissionControl() {
		int port = Integer.parseInt(System.getProperty("port", "8888"));
		return new RemoteMissionControl(port);
	}

}
