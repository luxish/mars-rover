package com.nasa.mars.mission.scenario;

import com.nasa.mars.mission.location.Direction;
import com.nasa.mars.mission.location.Position;

/**
 * A schematic for the behavior of a mechanized robot that can navigate a 2D surface.
 * 
 * @author Lucian M
 *
 */
public interface Rover {
	
	/**
	 * Action that is used to turn the robot 90 degrees to the right.
	 */
	void turnRight();
	
	/**
	 * Action that is used to turn the robot 90 degrees to the left.
	 */
	void turnLeft();
	
	/**
	 * Action that is used to move the robot exactly one unit in the direction it's facing.
	 */
	void move();
	
	/**
	 * Method used to gather data about the current direction of the robot.
	 * 
	 * @return {@link Direction}
	 */
	Direction getDirection();
	
	/**
	 * Method used to gather data about the current position of the robot.
	 * 
	 * @return {@link Position}
	 */
	Position getPosition();

}
