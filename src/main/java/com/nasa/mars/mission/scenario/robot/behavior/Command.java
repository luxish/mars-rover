package com.nasa.mars.mission.scenario.robot.behavior;

/**
 * Command interface describes an abstract remote action.
 * 
 * @author Lucian M
 *
 */
public interface Command {
	
	/**
	 * @return true if the action was executed with success.
	 */
	boolean execute();

}
