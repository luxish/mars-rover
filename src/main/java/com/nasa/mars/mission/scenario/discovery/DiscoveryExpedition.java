package com.nasa.mars.mission.scenario.discovery;

import com.nasa.mars.mission.scenario.Rover;

/**
 * Division of the {@link DiscoveryDeploymentScenario} that keeps data about the mechanical robot and 
 * associated its commands.  
 * 
 * @author Lucian M
 *
 */
public class DiscoveryExpedition {
	
	private final Rover rover;
	
	private final String commands;
	
	public DiscoveryExpedition(Rover rover, String commands) {
		this.rover = rover;
		this.commands = commands;
	}
	
	public Rover getRover() {
		return rover;
	}
	
	public String getCommands() {
		return commands;
	}

}
