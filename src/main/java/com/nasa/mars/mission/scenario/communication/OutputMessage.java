package com.nasa.mars.mission.scenario.communication;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to store the output in a structure based on coordinates and direction. Output objects
 * contain a list of {@link OutputMessageChunk} and can be transformed into a single string line 
 * using the {@link Object#toString()} method. 
 * 
 * @author Lucian M
 *
 */
public class OutputMessage {
	
	private final List<OutputMessageChunk> chunks;
	
	public OutputMessage() {
		this.chunks = new ArrayList<>();
	}
	
	public List<OutputMessageChunk> getChunks() {
		return chunks;
	}
	
	public boolean addChunk(OutputMessageChunk omc) {
		return this.chunks.add(omc);
	}
	
	/**
	 * 
	 * Class used to map chunks of the {@link OutputMessage}.
	 * 
	 * @author Lucian M
	 *
	 */
	public static class OutputMessageChunk {
		
		public int posX;
		
		public int posY;
		
		public String direction;
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return posX + " " + posY + " "+ direction;
		}

	}
	
	/**
	 *  {@inheritDoc}
	 */
	@Override
	public String toString() {
		return chunks.stream()
				.map(Object::toString)
				.reduce("", (output, item) -> String.join(" ", output, item)).trim();
	}
	
}
