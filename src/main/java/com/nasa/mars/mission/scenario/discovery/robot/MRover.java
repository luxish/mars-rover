package com.nasa.mars.mission.scenario.discovery.robot;

import com.nasa.mars.mission.location.Direction;
import com.nasa.mars.mission.location.Position;
import com.nasa.mars.mission.location.PositionUtil;
import com.nasa.mars.mission.scenario.Rover;
import com.nasa.mars.mission.scenario.discovery.DiscoveryDeploymentScenario;

/**
 * Mechanical robot serves as an actor in the {@link DiscoveryDeploymentScenario}. 
 * 
 * @author Lucian M
 *
 */
public class MRover implements Rover {
	
	private Position position;
	
	private Direction direction;
	
	/**
	 * Creates a new mechanical robot.
	 * 
	 * @param position {@link Position}
	 * @param direction {@link Direction}
	 */
	public MRover(Position position, Direction direction) {
		this.position = position;
		this.direction = direction;
	}
	

	/**
	 * Gets the position of the object.
	 * 
	 * @return {@link Position}
	 */
	@Override
	public Position getPosition() {
		return position;
	}
	
	/**
	 * Gets the direction of the object.
	 * 
	 * @return {@link Direction}
	 */
	@Override
	public Direction getDirection() {
		return direction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void turnRight() {
		Direction newDir = Direction.getDirectionByCode(direction.getCode() + 1);
		if(newDir != null) {
			direction = newDir;				
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void turnLeft() {
		Direction newDir = Direction.getDirectionByCode(direction.getCode() - 1);
		if(newDir != null) {
			direction = newDir;			
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void move() {
		position = PositionUtil.createPositionWithDifference(position, PositionUtil.getDirectionOffset(direction));
	}

}
