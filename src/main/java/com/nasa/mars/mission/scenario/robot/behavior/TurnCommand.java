package com.nasa.mars.mission.scenario.robot.behavior;

import com.nasa.mars.mission.scenario.Rover;

/**
 * Command used by a remote entity to maneuver a {@link Rover} by changing its direction.  
 * 
 * @author Lucian M
 *
 */
public class TurnCommand implements Command {

	private final Turn turn;
	
	private final Rover rover;
	
	/**
	 * Creates a turn command for a specific rover. 
	 * 
	 * @param rover {@link Rover}
	 * @param turn {@link Turn} the target direction.  
	 */
	public TurnCommand(Rover rover, Turn turn) {
		this.turn = turn;
		this.rover = rover;
	}
	
	/**
	 * Execution method used dispatch the turning command to the rover.
	 */
	@Override
	public boolean execute() {
		if(turn == Turn.LEFT) {
			rover.turnLeft();
		} else if(turn == Turn.RIGHT){
			rover.turnRight();
		}
		return true;
	}

	/**
	 * Enumeration used to define the {@link TurnCommand}.
	 * 
	 * @author Lucian M
	 *
	 */
	public static enum Turn {
		LEFT, RIGHT
	}

}
