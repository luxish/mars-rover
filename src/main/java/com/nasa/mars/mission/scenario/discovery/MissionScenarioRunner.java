package com.nasa.mars.mission.scenario.discovery;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.nasa.mars.mission.location.Direction;
import com.nasa.mars.mission.location.Position;
import com.nasa.mars.mission.location.SurfaceMap;
import com.nasa.mars.mission.scenario.DeploymentScenario;
import com.nasa.mars.mission.scenario.Rover;
import com.nasa.mars.mission.scenario.communication.InputMessage;
import com.nasa.mars.mission.scenario.communication.InputMessage.InputMessageChunk;
import com.nasa.mars.mission.scenario.discovery.robot.MRover;

/**
 * The mission control class used to setup and run a rover deployment scenario. 
 * 
 * @author Lucian M
 *
 */
public class MissionScenarioRunner {
	
	/**
	 * Method used to setup and run the mission scenario.
	 * 
	 * @param inputMessage
	 * @return
	 */
	public DeploymentScenario setupDeploymentScenario(InputMessage inputMessage) {
		Set<Position> initPositions = new HashSet<>();
		
		SurfaceMap surfaceMap = createSurfaceMap(inputMessage.getInitMapParams());
		DeploymentScenario deployment = new DiscoveryDeploymentScenario(surfaceMap);
		
		inputMessage.getChunks().forEach(item -> {
			// Try to create a rover based on an input message chunk.
			Rover rover = createRover(initPositions, surfaceMap, item);
			
			// Add the Rover only if it's valid.
			if(rover != null) {
				initPositions.add(rover.getPosition());
				deployment.addRover(rover, item.commands);
			}
		});
		return deployment;
	}

	/**
	 * Method used to create {@link Rover} objects if the validations pass. Rovers that can collide with others or
	 * are off the map will be lost. 
	 * 
	 * @param initPositions initialized positions
	 * @param surfaceMap 
	 * @param item {@link InputMessageChunk}
	 * @return {@link Rover} implementation.
	 */
	protected static Rover createRover(Set<Position> initPositions, SurfaceMap surfaceMap, InputMessageChunk item) {
		Position position = extractMapPosition(item, surfaceMap);
		Direction direction = extractDirection(item.direction);
		if(position == null || initPositions.contains(position) || direction == null) {
			return null;
		}
		return new MRover(position, direction);
	}

	/**
	 * Method used to validate the {@link Position} object taken from InputMessageChunk. 
	 * 
	 * @param item {@link InputMessageChunk}
	 * @param surfaceMap {@link SurfaceMap}
	 * @return {@link Position}
	 */
	private static Position extractMapPosition(InputMessageChunk item, SurfaceMap surfaceMap) {
		Position position = new Position(item.posX, item.posY);
		if(surfaceMap.hasCollision(position)) {
			return null;
		}
		return position;
	}

	/**
	 * Try to map a direction string to a value from the {@link Direction} enum.
	 * 
	 * @param dirStr String should be N, E, S, W
	 * @return {@link Direction} according to the letter, null if the provided string did not match.
	 */
	private static Direction extractDirection(String dirStr) {
		Optional<Direction> direction = Arrays.asList(Direction.values()).stream()
				.filter(item -> item.name().equals(dirStr))
				.findFirst();
		return direction.orElse(null);
	}

	/**
	 * Method used to create a {@link SurfaceMap} from the provided init params.
	 * 
	 * @param initParams
	 * @return
	 */
	private static SurfaceMap createSurfaceMap(int[] initParams) {
		return new SurfaceMap(initParams[0], initParams[1]);
	}
	
}
