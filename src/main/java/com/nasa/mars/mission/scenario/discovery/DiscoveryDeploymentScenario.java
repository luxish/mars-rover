package com.nasa.mars.mission.scenario.discovery;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.nasa.mars.mission.location.Position;
import com.nasa.mars.mission.location.SurfaceMap;
import com.nasa.mars.mission.scenario.DeploymentScenario;
import com.nasa.mars.mission.scenario.Rover;
import com.nasa.mars.mission.scenario.communication.OutputMessage;
import com.nasa.mars.mission.scenario.communication.OutputMessage.OutputMessageChunk;
import com.nasa.mars.mission.scenario.robot.behavior.Command;
import com.nasa.mars.mission.scenario.robot.behavior.MoveCommand;
import com.nasa.mars.mission.scenario.robot.behavior.TurnCommand;

/**
 * Deployment scenario used to coordinate and maneuver multiple expeditions. An expedition is 
 * a movement plan of a rover and is defined as a {@link DiscoveryExpedition} object. the discovery
 * expedition states that expeditions are done sequential in the order that were added.     
 * 
 * @author Lucian M
 */
public class DiscoveryDeploymentScenario implements DeploymentScenario {
	
	private static final char MOVE_CMD = 'M';

	private static final char RIGHT_CMD = 'R';
	
	private static final char LEFT_CMD = 'L';

	private final SurfaceMap surfaceMap;
	
	private final List<DiscoveryExpedition> expeditions;
	
	/**
	 * Creates an instance of the {@link DiscoveryDeploymentScenario} based on the provided surface.
	 * 
	 * @param surfaceMap {@link SurfaceMap}
	 */
	public DiscoveryDeploymentScenario(SurfaceMap surfaceMap) {
		this.surfaceMap = surfaceMap;
		this.expeditions = new ArrayList<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addRover(Rover rover, String commands) {
		return expeditions.add(new DiscoveryExpedition(rover, commands));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputMessage runExpeditions() {
		OutputMessage expeditionsResult =  new OutputMessage();
		expeditions.forEach(expedition -> {
			Rover rover = expedition.getRover();
			String commands = expedition.getCommands();
			
			// Dispatch every command in the provided order.
			for(int idx= 0; idx < commands.length(); idx++) {
				dispatchCommand(rover, commands.charAt(idx));
			}
			// Mark the position of the rover on the surface map. This way collisions are prevented.
			surfaceMap.markPositionTaken(rover.getPosition());
			
			// Add output chunk to the result object
			expeditionsResult.addChunk(createOutputChunk(rover));
		});
		
		return expeditionsResult;
	}

	/**
	 * Method used to dispatch commands based on a command code.
	 * 
	 * @param rover {@link Rover}
	 * @param commandCode character 
	 */
	private void dispatchCommand(Rover rover, char commandCode) {
		Command command = null;
		switch (commandCode) {
			case RIGHT_CMD: 
				command = new TurnCommand(rover, TurnCommand.Turn.RIGHT); 
				break;
			case LEFT_CMD: 
				command = new TurnCommand(rover, TurnCommand.Turn.LEFT); 
				break;
			case MOVE_CMD: 
				command = new MoveCommand(rover, hasCollisionFunction(surfaceMap)); 
				break;
			default: break;
		}
		
		if(command != null) {
			command.execute();
		}
	}
	
	/**
	 * Method that creates a check collision function based on the surface map.
	 * 
	 * @param surfaceMap {@link SurfaceMap}
	 * @return Function<Position, Boolean>
	 */
	private Function<Position, Boolean> hasCollisionFunction(SurfaceMap surfaceMap) {
		return (position) -> surfaceMap.hasCollision(position);
	}
	
	/**
	 * Method used to retrieve all existent expeditions.  
	 * 
	 * @return List<DiscoveryExpedition>
	 */
	public List<DiscoveryExpedition> getExpeditions() {
		return expeditions;
	}
	
	/**
	 * Method used to get the surface map.
	 * 
	 * @return {@link SurfaceMap}
	 */
	public SurfaceMap getSurfaceMap() {
		return surfaceMap;
	}

	/**
	 * Method used to create an {@link OutputMessageChunk} object based on the rover state.
	 * 
	 * @param rover {@link Rover}
	 * @return {@link OutputMessageChunk}
	 */
	private OutputMessageChunk createOutputChunk(Rover rover) {
		OutputMessageChunk omc = new OutputMessageChunk();
		omc.posX = rover.getPosition().getX();
		omc.posY = rover.getPosition().getY();
		omc.direction = rover.getDirection().name();
		return omc;
	}
}
