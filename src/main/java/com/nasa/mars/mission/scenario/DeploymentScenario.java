package com.nasa.mars.mission.scenario;

import com.nasa.mars.mission.scenario.communication.OutputMessage;

/**
 * Deployment scenario used to define the basic actions needed to run a deployment plan. 
 * 
 * @author Lucian M
 *
 */
public interface DeploymentScenario {

	/**
	 * Scenario action used to add {@link Rover} entities to the plan. Those Rovers are 
	 * actually the actors of the plan.
	 * 
	 * @param rover {@link Rover}
	 * @param commands {@link String}
	 * @return true if the rover was added.
	 */
	boolean addRover(Rover rover, String commands);

	/**
	 * Scenario action that triggers the start of the scenario. Once the signal is received, 
	 * all actors will execute their related commands. 
	 * 
	 * @return A string that would declare the final position and direction of the actors. 
	 */
	OutputMessage runExpeditions();
	
	
}
