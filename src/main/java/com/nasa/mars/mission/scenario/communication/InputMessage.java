package com.nasa.mars.mission.scenario.communication;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to map the input message into initialization parameters for the scenario surface and 
 * pairs of coordinates, positions, and commands for the scenario actors.
 * 
 * @author Lucian M
 *
 */
public class InputMessage {

	private int[] initMapParams;
	
	private List<InputMessageChunk> chunks;
	
	public InputMessage() {
		chunks = new ArrayList<>();
	}

	public int[] getInitMapParams() {
		return initMapParams;
	}

	public void setInitMapParams(int[] initMapParams) {
		this.initMapParams = initMapParams;
	}

	public List<InputMessageChunk> getChunks() {
		return chunks;
	}

	public void setChunks(List<InputMessageChunk> chunks) {
		this.chunks = chunks;
	} 
	
	public boolean addChunk(InputMessageChunk chunk) {
		return chunks.add(chunk);
	}
	
	/**
	 * Class used to map chunks of the {@link InputMessage}.
	 * 
	 * @author Lucian M
	 *
	 */
	public static class InputMessageChunk {
		
		public int posX;
		
		public int posY;
		
		public String direction;
		
		public String commands;

	}
}
