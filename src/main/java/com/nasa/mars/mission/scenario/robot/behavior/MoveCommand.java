package com.nasa.mars.mission.scenario.robot.behavior;

import java.util.function.Function;

import com.nasa.mars.mission.location.Direction;
import com.nasa.mars.mission.location.Position;
import com.nasa.mars.mission.location.PositionUtil;
import com.nasa.mars.mission.scenario.Rover;

/**
 * Command used by a remote entity to maneuver a {@link Rover} by moving it into the current direction.   
 * 
 * @author Lucian M
 *
 */
public class MoveCommand implements Command {
	
	private final Rover rover;
	
	private final Function<Position, Boolean> hasCollision;

	/**
	 * Creates an instance of the "move" command.
	 * 
	 * @param rover {@link Rover}
	 * @param hasCollision function that takes a position as argument and returns a {@link Boolean} object. it is used to 
	 * check collisions at some point in the execute method .  
	 */
	public MoveCommand(Rover rover, Function<Position, Boolean> hasCollision) {
		this.rover = rover;
		this.hasCollision = hasCollision;
	}

	/**
	 * Execution method used to check the map if the desired position is vacant and moves the rover.
	 */
	@Override
	public boolean execute() {
		Position futurePosition = computeFuturePosition(rover.getPosition(), rover.getDirection());
		if(hasCollision.apply(futurePosition)) {
			System.out.println("Collision on position "  + futurePosition.getX() + " " + futurePosition.getY());
			return false;
		}
		rover.move();
		return true;
	}

	/**
	 * Method used to compute the future position based on its current position and direction. 
	 * 
	 * @param position {@link Position}
	 * @param direction {@link Direction}
	 * @return {@link Position} the new position.
	 */
	private Position computeFuturePosition(Position position, Direction direction) {
		return PositionUtil.createPositionWithDifference(position, PositionUtil.getDirectionOffset(direction));
	}
}
