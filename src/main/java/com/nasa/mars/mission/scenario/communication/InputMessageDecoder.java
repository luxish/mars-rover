package com.nasa.mars.mission.scenario.communication;

import com.nasa.mars.mission.scenario.communication.InputMessage.InputMessageChunk;

/**
 * Decoder class used to transform the input parameters into the correct structure.
 * The integrity of the data is NOT checked.
 * 
 * @author Lucian M
 *
 */
public class InputMessageDecoder {

	private static final String NO_INPUT_STRING_MESSAGE = "No input string provided.";

	private static final String NMBER_OF_ARGS_MESSAGE = "The number of arguments does not meet the specification. Input tokens=%s";
	
	private static final String INT_ARG_EXPECTED_MESSAGE = "Expected input parameter of type int. Got %s";

	private InputMessageDecoder() {
	}
	
	/**
	 * Method used to parse the input and create a {@link InputMessage} object out of it. 
	 * In case of incorrect structure, a runtime exception will be thrown.
	 * 
	 * @param input {@link String}
	 * @return  {@link InputMessage} if the structure is correct.
	 */
	public static InputMessage parseMessage(String inputString) {
		if(inputString == null) {
			throw new IllegalArgumentException(NO_INPUT_STRING_MESSAGE);
		}
		return parseMessage(inputString.trim().split(" "));
	}
	
	/**
	 * Method used to parse the input and create a {@link InputMessage} object out of it. 
	 * In case of incorrect structure, a runtime exception will be thrown.
	 * 
	 * @param input String[] the input array
	 * @return  {@link InputMessage} if the structure is correct.
	 */
	public static InputMessage parseMessage(String ...input) {
		validateInputLength(input);
		
		int idx = 0;
		InputMessage message = new InputMessage();
		
		int wInput = parseAndValidateIntValue(input[idx++]);
		int hInput = parseAndValidateIntValue(input[idx++]);
		message.setInitMapParams(new int[] { wInput, hInput});
		
		while(idx < input.length) {
			InputMessageChunk chunk = new InputMessageChunk();
			chunk.posX = parseAndValidateIntValue(input[idx++]);
			chunk.posY = parseAndValidateIntValue(input[idx++]);
			chunk.direction = input[idx++];
			chunk.commands = input[idx++];
			message.addChunk(chunk);
		}
		
		return message;
		
	}

	/**
	 * Method used to validate the input length. It expects the first two arguments as the map length and a multiple 
	 * of 4 other arguments (position X, position Y, direction, commands) as rover initialization and behavior.
	 * 
	 * @param input String[]
	 */
	private static void validateInputLength(String... input) {
		if(input.length < 6 || (input.length - 2) % 4 != 0) {
			throw new IllegalArgumentException(String.format(NMBER_OF_ARGS_MESSAGE, input.length));
		}
	}
	
	/**
	 * Method used to parse an integer from the provided string value. 
	 * 
	 * @param value {@link String}
	 * @return int the parsed value
	 */
	private static int parseAndValidateIntValue(String value) {
		try {
			return Integer.parseInt(value);
		} catch(NumberFormatException ex) {
			throw new IllegalArgumentException(String.format(INT_ARG_EXPECTED_MESSAGE, value), ex);
		}
	}
}
